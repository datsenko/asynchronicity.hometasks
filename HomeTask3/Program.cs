﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace HomeTask3
{
    class Program
    {
        public static List<Employee> employees = new List<Employee>();

        public static void ReadData()
        {
            string path = Path.Combine(Environment.CurrentDirectory, @"data\company.csv");
            
            foreach(var data in File.ReadAllLines(path).Skip(1).Select(a => a.Split(',')))
            {
                employees.Add(new Employee
                {
                    FirstName = data[0],
                    LastName = data[1],
                    Department = data[2],
                    Age = int.Parse(data[3]),
                    Salary = int.Parse(data[4])
                });
            }
        }

        static void Main(string[] args)
        {
            for(int i = 0; i < 50000; i++) // 27 000 000 records, in this case PLinq is faster
                ReadData();
            Stopwatch sw = new Stopwatch();

            sw.Start();
            var departmentStats = employees
                .AsParallel()
                .GroupBy(e => e.Department, (department, salary) => new DepartmentStats
                {
                    Department = department,
                    AvgSalary = salary.Average(e => e.Salary)
                })
                .ToList();
            sw.Stop();

            var parallelCountingTime1 = sw.ElapsedMilliseconds;

            sw.Reset();
            sw.Start();
            var ageGroupsStat = employees
                .AsParallel()
                .GroupBy(e =>
                {
                    if (e.Age >= 17 && e.Age < 30)
                        return "From17To30";
                    else if (e.Age >= 30 && e.Age < 46)
                        return "From30To46";

                    return "From46To...";
                }, (age, salary) => new AgeGroupsStats { AgeGroup = age, MaxSalary = salary.Max(e => e.Salary) })
                .ToList();
            sw.Stop();

            var parallelCountingTime2 = sw.ElapsedMilliseconds;

            sw.Reset();
            sw.Start();
            var departmentStatsSync = employees
                .GroupBy(e => e.Department, (department, salary) => new DepartmentStats
                {
                    Department = department,
                    AvgSalary = salary.Average(e => e.Salary)
                })
                .ToList();
            sw.Stop();

            var syncCounting1 = sw.ElapsedMilliseconds;

            sw.Reset();
            sw.Start();
            var ageGroupsStatSync = employees
                .GroupBy(e =>
                {
                    if (e.Age >= 17 && e.Age < 30)
                        return "From17To30";
                    else if (e.Age >= 30 && e.Age < 46)
                        return "From30To46";

                    return "From46To...";
                }, (age, salary) => new AgeGroupsStats { AgeGroup = age, MaxSalary = salary.Max(e => e.Salary) })
                .ToList();
            sw.Stop();

            var syncCounting2 = sw.ElapsedMilliseconds;

            foreach (var stat in departmentStats)
            {
                Console.WriteLine($"{stat.Department} : {stat.AvgSalary}");
            }
            Console.WriteLine($"Time -> {parallelCountingTime1}");
            Console.WriteLine($"Sync counting time-> {syncCounting1}");
            Console.WriteLine("***************************************");

            foreach (var stat in ageGroupsStat)
            {
                Console.WriteLine($"{stat.AgeGroup} : {stat.MaxSalary}");
            }
            Console.WriteLine($"Time -> {parallelCountingTime2}");
            Console.WriteLine($"Sync counting time -> {syncCounting2}");
            Console.WriteLine("***************************************");
        }
    }
}
