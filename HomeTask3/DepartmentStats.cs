﻿namespace HomeTask3
{
    public class DepartmentStats
    {
        public string Department { get; set; }
        public double AvgSalary { get; set; }
    }
}
