﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace HomeTask2
{
    class Program
    {
        public const int upTo = 100000000;
        public static int threadsNumber = 3;
        public static HashSet<int> PrimeNumbersSet = new HashSet<int>();
        public static List<Parameters> threadsParameters = new List<Parameters>();
        public static List<Thread> threads = new List<Thread>();
        public static object lockInstance = new object();

        public static bool isPrimeNumber(int number)
        {
            int counter = 0;
            int sqrtN = (int)Math.Sqrt(number);
            for(int i = 1; i <= sqrtN; i++ )
            {
                if (number % i == 0)
                    counter++;

                if (counter > 1)
                    return false;
            }

            return true;
        }

        public static void PopulateSet(object data)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var param = (Parameters)data;
            for(int i = param.Start; i < param.End; i++)
            {
                if (isPrimeNumber(i))
                {
                    lock(lockInstance)
                    {
                        PrimeNumbersSet.Add(i);
                    }
                }
            }
            sw.Stop();
            Console.WriteLine($"In wait for {sw.ElapsedMilliseconds}");
        }

        public static void SetUpThreadsParameters()
        {
            int previous = 0;
            int part = upTo / threadsNumber;

            for(int i = 0; i < threadsNumber; i++)
            {
                var param = new Parameters(previous + 1, previous += part);
                threadsParameters.Add(param);
            }
        }

        public static void StartCalculation()
        {
            for(int i = 0; i < threadsNumber; i++)
            {
                var thread = new Thread(new ParameterizedThreadStart(PopulateSet));
                threads.Add(thread);
                thread.Start(threadsParameters[i]);
            }
        }

        static void Main(string[] args)
        {
            SetUpThreadsParameters();
            StartCalculation();
        }
    }
}
