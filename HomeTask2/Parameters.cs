﻿namespace HomeTask2
{
    public struct Parameters
    {
        public Parameters(int start, int end)
        {
            Start = start;
            End = end;
        }

        public int Start { get; set; }
        public int End { get; set; }
    }
}
