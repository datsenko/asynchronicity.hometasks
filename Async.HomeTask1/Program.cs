﻿using System;
using System.Threading;

namespace Async.HomeTask1
{
    class Program
    {
        public const int multiplier = 2;
        public const int upTo = 5;
        public static MultTable table;

        public static void BreakAtomacity()
        {
            table = new MultTable(2, 3, 4);
            Thread multiplyThread = new Thread(() =>
            {
                for (int i = 1; i <= upTo; i++)
                {
                    table.A *= multiplier;
                    Thread.Sleep(100);
                    table.B *= multiplier;
                    Thread.Sleep(100);
                    table.C *= multiplier;
                    Thread.Sleep(100);
                }
            });

            Thread showResultThread = new Thread(() =>
            {
                for (int i = 1; i <= upTo; i++)
                {
                    Console.WriteLine($"******* Iteration: {i} *******");
                    Console.WriteLine($"A = {table.A}");
                    Console.WriteLine($"B = {table.B}");
                    Console.WriteLine($"C = {table.C}");
                    Thread.Sleep(400);
                }
            });

            multiplyThread.Start();
            Thread.Sleep(100);
            showResultThread.Start();
            multiplyThread.Join();
            showResultThread.Join();
        }

        public static void MakeRaceCondition()
        {
            CakeMaker cook = new CakeMaker();
            Thread prepareThread = new Thread(cook.PrepareDough);
            Thread addSomeThread = new Thread(cook.AddSome);
            Thread bakeThread = new Thread(cook.BakeCake);
            prepareThread.Start();
            addSomeThread.Start();
            bakeThread.Start();
            prepareThread.Join();
            addSomeThread.Join();
            bakeThread.Join();
        }

        static void Main(string[] args)
        {
            BreakAtomacity();
            Console.ReadLine();
            Console.Clear();

            MakeRaceCondition();
            Console.ReadLine();
            Console.Clear();

            MakeRaceCondition();
            Console.ReadLine();
            Console.Clear();

            MakeRaceCondition();
        }
    }
}
