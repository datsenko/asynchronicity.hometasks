﻿namespace Async.HomeTask1
{
    public struct MultTable
    {
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }

        public MultTable(int numberA, int numberB, int numberC)
        {
            A = numberA;
            B = numberB;
            C = numberC;
        }
    }
}
