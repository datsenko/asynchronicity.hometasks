﻿using System;

namespace Async.HomeTask1
{
    public class CakeMaker
    {
        public string firstStep = "Take a powder";
        public string secondStep = "Make a dough";
        public string thirdStep = "Add some fruits";
        public string fourthStep = "Put cake into the oven";
        public string fifthStep = "Turn on the oven";

        public void PrepareDough()
        {
            Console.WriteLine($"1.{firstStep} and 2.{secondStep}");
        }

        public void AddSome()
        {
            Console.WriteLine($"3.{thirdStep}");
        }

        public void BakeCake()
        {
            Console.WriteLine($"4.{fourthStep} and 5.{firstStep}");
        }
    }
}
